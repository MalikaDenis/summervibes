
# Summer vibes: 
## Am I right to be upset by this summer weather?

## Summary: 
As summer ends up in Tours, France, I feel upset and frustrated. I have the feeling that this summer was not a genuine summer. However, I read that the statistics are not bad compared to the mean values. Should I trust the news? Well, no. Let us make us of my data science skills and check those assertions!


Besides, I have lived in different cities in the last years and I want to compare the weather statistics in those cities, namely Berlin(Ge), Groningen(Nl) and Tours(Fr). Hence, I will check my feelings that the weather is much poorer in some cities (Sorry Dutch people but you know I am right...).  

## Scheme:
### Importation and cleaning
- Import csv files and format the dataframe
- Get monthly and yearly summer statistics

### Comparison of the three city summer data
- \# days over 25°C
- amount of precipitation
- conclusion: where should have I spent my last 5 summers?

### Tours summer weather analysis
- Graphical analysis: comparison of 2021 data with the statistics over the last 20/30 years 
    - bar plots
    - histograms
    - ecdf
    
### Correlation
- Potential correlations between daily features.
    - pairplot
    - heatmap
    - conclusion: can we apply Machine Learning? 
- "Noël au balcon, Pâques aux tisons ?"   
    - Correlation analysis of Christmas and Easter temperatures.
    - Null hypothesis.

### Machine Learning    
- Supervised classification KNN (monthly data -> season)
- K-means clustering (months clustered into 'seasons')



```python
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from dateutil.easter import *
```

## Importation and cleaning
### Import data
We are interested in historical daily meteorological data of some european cities. 
We are getting them from the European Climate Assessment & Dataset project, ECAD
(https://www.ecad.eu/). The different parameters are given in independent files that will need to be merged. We stored the files in a directory 'sources'. 
We make a list of the parameters of interest, namely max, min and mean temperatures, precipitation amount and sunshine hours.  


```python
#list of files of interest
plist=['TX','TN','TG','RR','HU','SS','QQ','PP']
#Source: ecad
path='sources/'
```

Let us have a look at the files.
The first 18 lines are description so we need to skip them.
The header contains spaces that we should discard.
There are 4 columns:
'SOUID,    DATE,   TX, Q_TX'.
SOUID gives information on the source, date is self-explanatory, Tx, here is the parameter and Q_ is a quality code of the previous value. Only the date and the parameter value will be useful in our study.



```python
#Import every file corresponding to the parameters listed in li, 
#convert them in dataframes and compile them in a list.
lidf=[pd.read_csv(path+f+'_STAID002190.csv',skipinitialspace=True, skiprows=18) for f in plist]
#Merge all the parameter dataframe into one global dataframe on the date column.
dftest=lidf[0].drop(['SOUID'],1)
for i in lidf[1:]:
    dftest=dftest.merge(i.drop(['SOUID'],1), on=['DATE'])
#Drop quality columns.    
dftest.drop(dftest.columns[dftest.columns.str.startswith('Q_')],1,inplace=True)    
display(dftest.head())
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>DATE</th>
      <th>TX</th>
      <th>TN</th>
      <th>TG</th>
      <th>RR</th>
      <th>HU</th>
      <th>SS</th>
      <th>QQ</th>
      <th>PP</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>19610101</td>
      <td>75</td>
      <td>-14</td>
      <td>30</td>
      <td>82</td>
      <td>92</td>
      <td>46</td>
      <td>54</td>
      <td>-9999</td>
    </tr>
    <tr>
      <th>1</th>
      <td>19610102</td>
      <td>109</td>
      <td>22</td>
      <td>66</td>
      <td>364</td>
      <td>95</td>
      <td>0</td>
      <td>16</td>
      <td>-9999</td>
    </tr>
    <tr>
      <th>2</th>
      <td>19610103</td>
      <td>72</td>
      <td>35</td>
      <td>54</td>
      <td>41</td>
      <td>88</td>
      <td>12</td>
      <td>33</td>
      <td>-9999</td>
    </tr>
    <tr>
      <th>3</th>
      <td>19610104</td>
      <td>82</td>
      <td>22</td>
      <td>52</td>
      <td>18</td>
      <td>90</td>
      <td>42</td>
      <td>53</td>
      <td>-9999</td>
    </tr>
    <tr>
      <th>4</th>
      <td>19610105</td>
      <td>72</td>
      <td>-3</td>
      <td>34</td>
      <td>94</td>
      <td>87</td>
      <td>50</td>
      <td>57</td>
      <td>-9999</td>
    </tr>
  </tbody>
</table>
</div>


The documentation of the data states that the temperatures are given in 0.1°C, the precipitation amount in 0.1mm and the sunshine hours in 0.1hour. Besides, missing values are filled by -9999.
Let's check by displaying information of the dataframe.


```python
display(dftest.describe())
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>DATE</th>
      <th>TX</th>
      <th>TN</th>
      <th>TG</th>
      <th>RR</th>
      <th>HU</th>
      <th>SS</th>
      <th>QQ</th>
      <th>PP</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>2.215800e+04</td>
      <td>22158.00000</td>
      <td>22158.000000</td>
      <td>22158.000000</td>
      <td>22158.000000</td>
      <td>22158.000000</td>
      <td>22158.000000</td>
      <td>22158.000000</td>
      <td>22158.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>1.990902e+07</td>
      <td>-23.37192</td>
      <td>-108.047567</td>
      <td>-65.713151</td>
      <td>-162.669555</td>
      <td>-102.991470</td>
      <td>-2711.228044</td>
      <td>142.418901</td>
      <td>7481.796146</td>
    </tr>
    <tr>
      <th>std</th>
      <td>1.751340e+05</td>
      <td>1351.28567</td>
      <td>1338.884985</td>
      <td>1344.935534</td>
      <td>1336.050769</td>
      <td>1338.461888</td>
      <td>4485.886058</td>
      <td>341.152417</td>
      <td>6853.945854</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.961010e+07</td>
      <td>-9999.00000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
      <td>-9999.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>1.976030e+07</td>
      <td>101.00000</td>
      <td>29.000000</td>
      <td>67.000000</td>
      <td>0.000000</td>
      <td>70.000000</td>
      <td>-9999.000000</td>
      <td>56.000000</td>
      <td>10077.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>1.991050e+07</td>
      <td>154.00000</td>
      <td>74.000000</td>
      <td>114.000000</td>
      <td>0.000000</td>
      <td>80.000000</td>
      <td>15.000000</td>
      <td>127.000000</td>
      <td>10158.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>2.006070e+07</td>
      <td>216.00000</td>
      <td>115.000000</td>
      <td>164.000000</td>
      <td>16.000000</td>
      <td>88.000000</td>
      <td>67.000000</td>
      <td>221.000000</td>
      <td>10214.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>2.021083e+07</td>
      <td>408.00000</td>
      <td>226.000000</td>
      <td>312.000000</td>
      <td>620.000000</td>
      <td>100.000000</td>
      <td>160.000000</td>
      <td>1424.000000</td>
      <td>10450.000000</td>
    </tr>
  </tbody>
</table>
</div>


The max value of TX confirms the assertion that temperatures are given in 0.1°C. We also notice the -9999 min value. 
Therefore the missing values should be replaced by the conventional NaN and the parameter values divided by 10. 
Moreover the date column is of integer type and we would rather convert it in datetime format.


```python
dftest.replace(-9999,np.nan, inplace=True)
dftest[plist]=dftest[plist]/10
dftest['DATE']=pd.to_datetime(dftest['DATE'], format='%Y%m%d')
display(dftest.head())
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>DATE</th>
      <th>TX</th>
      <th>TN</th>
      <th>TG</th>
      <th>RR</th>
      <th>HU</th>
      <th>SS</th>
      <th>QQ</th>
      <th>PP</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1961-01-01</td>
      <td>7.5</td>
      <td>-1.4</td>
      <td>3.0</td>
      <td>8.2</td>
      <td>9.2</td>
      <td>4.6</td>
      <td>5.4</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1961-01-02</td>
      <td>10.9</td>
      <td>2.2</td>
      <td>6.6</td>
      <td>36.4</td>
      <td>9.5</td>
      <td>0.0</td>
      <td>1.6</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1961-01-03</td>
      <td>7.2</td>
      <td>3.5</td>
      <td>5.4</td>
      <td>4.1</td>
      <td>8.8</td>
      <td>1.2</td>
      <td>3.3</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1961-01-04</td>
      <td>8.2</td>
      <td>2.2</td>
      <td>5.2</td>
      <td>1.8</td>
      <td>9.0</td>
      <td>4.2</td>
      <td>5.3</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1961-01-05</td>
      <td>7.2</td>
      <td>-0.3</td>
      <td>3.4</td>
      <td>9.4</td>
      <td>8.7</td>
      <td>5.0</td>
      <td>5.7</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>


Now, we write a function to import and clean data from any source.


```python
def importdata(source):
    lidf=[pd.read_csv(path+f+source,skipinitialspace=True, skiprows=18) for f in plist]
    df=lidf[0].drop(['SOUID'],1)
    for i in lidf[1:]:
        df=df.merge(i.drop(['SOUID'],1), on=['DATE'])
    df.drop(df.columns[df.columns.str.startswith('Q_')],1,inplace=True)
    df['HU'].replace([0,1],np.NaN, inplace=True)
    df['HU']=df['HU']*10
    df['DATE']=pd.to_datetime(df['DATE'], format='%Y%m%d')
    df.replace(-9999,np.nan, inplace=True)
    df[plist]=df[plist]/10 
    return df
```

Let us apply our function to import Tours(fr), Groningen(nl) and Berlin(ge) weather history.


```python
tours=importdata('_STAID002190.csv')
groningen=importdata('_STAID000163.csv')
berlin=importdata('_STAID004570.csv')
```

### Monthly statistics
Since global warming and climate change are a thing, it would not make sense to compare last summmer with statistics over more than a century so one would be willing to restrict the study of the abnormality of summer 2021 to the recent years.
Also, we are interested by monthly statistics so we resample the dataframe on monthly basis. For this purpose, we need to choose the aggregation methods to use for each parameter. We also count days where temperature reaches 25°C. 
Finally, as the scope of our study is mainly the summer weather, we will isolate summer months from the other by adding a boolean type column 'summer'. Here we consider only july and august as the summer months.  


```python
#General function to get monthly statistics of a given city over a given period of time.
def monthstat(city, start):#City dataframe is assumed loaded.
    #select data after a given year 'start', set 'DATE' column as index
    #and resample it to get monthly values. Aggregation functions are given.
    #We also count days where temperature reaches 25°C.
    dfmonthly=city[city['DATE'].dt.year>=start].set_index('DATE').resample('M')\
    .agg({'TX':['mean',lambda ts: (ts > 25).sum()],'TN':'mean','TG':'mean',\
          'RR':'sum','HU':'mean','SS':'sum','QQ':'sum','PP':'mean'})
    #Flatten column index and rename explictly.
    dfmonthly.columns=['TX_mean','Warm days','TN_mean','TG_mean','RR_sum','HU_mean','SS_sum',\
                      'QQ_sum','PP_mean']
    #Identify summer months
    dfmonthly['summer']=dfmonthly.index.month.isin([7,8])
    return dfmonthly
```

Let's have a look at the Groningen monthly dataframe.


```python
gronmont=monthstat(groningen,2000)
print('Groningen monthly')
display(gronmont.head())
```

    Groningen monthly



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TX_mean</th>
      <th>Warm days</th>
      <th>TN_mean</th>
      <th>TG_mean</th>
      <th>RR_sum</th>
      <th>HU_mean</th>
      <th>SS_sum</th>
      <th>QQ_sum</th>
      <th>PP_mean</th>
      <th>summer</th>
    </tr>
    <tr>
      <th>DATE</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2000-01-31</th>
      <td>6.051613</td>
      <td>0.0</td>
      <td>1.412903</td>
      <td>4.032258</td>
      <td>56.7</td>
      <td>90.709677</td>
      <td>53.4</td>
      <td>74.6</td>
      <td>1021.106452</td>
      <td>False</td>
    </tr>
    <tr>
      <th>2000-02-29</th>
      <td>8.368966</td>
      <td>0.0</td>
      <td>2.293103</td>
      <td>5.313793</td>
      <td>81.3</td>
      <td>87.068966</td>
      <td>96.2</td>
      <td>156.9</td>
      <td>1015.648276</td>
      <td>False</td>
    </tr>
    <tr>
      <th>2000-03-31</th>
      <td>9.190323</td>
      <td>0.0</td>
      <td>3.025806</td>
      <td>6.232258</td>
      <td>100.0</td>
      <td>87.322581</td>
      <td>64.8</td>
      <td>213.3</td>
      <td>1017.706452</td>
      <td>False</td>
    </tr>
    <tr>
      <th>2000-04-30</th>
      <td>14.483333</td>
      <td>1.0</td>
      <td>4.250000</td>
      <td>9.460000</td>
      <td>45.9</td>
      <td>79.400000</td>
      <td>146.2</td>
      <td>435.2</td>
      <td>1008.140000</td>
      <td>False</td>
    </tr>
    <tr>
      <th>2000-05-31</th>
      <td>19.106452</td>
      <td>4.0</td>
      <td>8.096774</td>
      <td>13.745161</td>
      <td>83.7</td>
      <td>77.354839</td>
      <td>221.2</td>
      <td>627.3</td>
      <td>1014.974194</td>
      <td>False</td>
    </tr>
  </tbody>
</table>
</div>


### Yearly summer statistics
To facilitate our study, we might be willing to merge both summer months to compare years.
In this aim, let's resample summer months (july and august) on year. We take the mean daily max, average, and min temperature and the cumulative of daily precipitation and sunshine hours.

To compare the statistics of a specific summer with the average values, we define two new columns 'Temperature' and 'Rain'. 'Temperature' compares the number of warm days (temperature>25°C) of the summer with the average number, 'Cold' and 'Hot' meaning a summer with respectively less, more warm days than usual.
As for 'Rain', it accounts for the quantity of precipitation during the considered summer with respect to the average summer value, 'Wet' for a rainier summer and 'Dry' for a less rainy summer. 

A general function taking as inputs the dataframe of the city as imported with the *importdata* function and the year from which we start the study. 


```python
#General function to get summer statistics of a given city over a given period of time.
def summerstat(city, start):
    dfmonthly=monthstat(city,start)
    dfsum=dfmonthly[dfmonthly.summer==True].resample('Y')\
    .agg({'TX_mean':'mean','Warm days':'sum','TN_mean':'mean',\
          'TG_mean':'mean', 'RR_sum':'sum','SS_sum':'sum'})
    dfsum['Temperature']=(dfsum['Warm days']<dfsum['Warm days'].mean()).map({True:'Cold',False:'Hot'})
    dfsum['Rain']=(dfsum['RR_sum']>dfsum['RR_sum'].mean()).map({True:'Wet',False:'Dry'})
    return dfsum

gronmont=monthstat(groningen,2000)
toursmonthly=monthstat(tours,2000)
gronsum=summerstat(groningen,2000)
berlinsum=summerstat(berlin,2000)
tourssum=summerstat(tours,2000)
display('Groningen summer statistics')
display(gronsum.head())

```


    'Groningen summer statistics'



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TX_mean</th>
      <th>Warm days</th>
      <th>TN_mean</th>
      <th>TG_mean</th>
      <th>RR_sum</th>
      <th>SS_sum</th>
      <th>Temperature</th>
      <th>Rain</th>
    </tr>
    <tr>
      <th>DATE</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2000-12-31</th>
      <td>20.595161</td>
      <td>3.0</td>
      <td>10.956452</td>
      <td>15.750000</td>
      <td>145.5</td>
      <td>282.3</td>
      <td>Cold</td>
      <td>Dry</td>
    </tr>
    <tr>
      <th>2001-12-31</th>
      <td>23.074194</td>
      <td>17.0</td>
      <td>12.403226</td>
      <td>17.843548</td>
      <td>200.2</td>
      <td>404.8</td>
      <td>Hot</td>
      <td>Wet</td>
    </tr>
    <tr>
      <th>2002-12-31</th>
      <td>22.991935</td>
      <td>15.0</td>
      <td>13.388710</td>
      <td>18.112903</td>
      <td>224.2</td>
      <td>332.7</td>
      <td>Hot</td>
      <td>Wet</td>
    </tr>
    <tr>
      <th>2003-12-31</th>
      <td>24.554839</td>
      <td>23.0</td>
      <td>12.570968</td>
      <td>18.609677</td>
      <td>95.0</td>
      <td>468.7</td>
      <td>Hot</td>
      <td>Dry</td>
    </tr>
    <tr>
      <th>2004-12-31</th>
      <td>22.335484</td>
      <td>16.0</td>
      <td>12.195161</td>
      <td>17.161290</td>
      <td>310.2</td>
      <td>410.4</td>
      <td>Hot</td>
      <td>Wet</td>
    </tr>
  </tbody>
</table>
</div>


### Comparison of the three cities summer data

As we wish to compare the summer weather of the three cities, we aim to join the yearly summer statistic of a given parameter of the three cities. 



```python
def multicities(parameter,year,title):
    all=pd.concat([tourssum[[parameter]],berlinsum[[parameter]],gronsum[[parameter]]],\
                  keys=['tours','berlin','gron'],axis=1)
    all.set_index(all.index.year, inplace=True)
    all.columns=['Tours','Berlin','Groningen']
    all[all.index>year].plot(kind='bar',width=0.8)   
    plt.title(title)
    plt.ylabel(parameter)
```

We compare the quantity of precipitation and the number of warm days. It would have been interesting to compare the sunshine hours, however this data was not available for Tours for the considered years.


```python
multicities('RR_sum',2014,'Cumulation of precipitation during summer')
multicities('Warm days',2014,'Number of warm days during summer')
```


![png](output_26_0.png)



![png](output_26_1.png)


*Conclusion: where should have I spent my last 5 summers?*

Based on the graphs above and my personal likes, I should be able to make a choice on the city that would suit me most.    
First, despite my love for the beautiful city of Groningen, I must say that the small number of warm days is an insurmountable flaw. Then, if we consider that less rain and more warm days, the better, Tours is preferable. However, since the number of warm days is the essential criterion for me, I should have spent 2021 summer in Berlin. 

### Tours summer weather analysis


With the *summerstat* function defined above, we aim to compare the 2021 summer with the last twenty summers. We focus on two weather parameters the number of warm days and the volume of rainfalls.  
They are plotted below and the color accounts for the comparison with respect to the mean value on the twenty year period.  


```python
plt.figure(figsize = (6,4))
meanWarm=tourssum['Warm days'].mean()
fig=sns.barplot(x=tourssum.index.year,y='Warm days', hue='Temperature', data=tourssum, dodge=False)
fig.axhline(meanWarm, color='purple')
plt.xticks(rotation=45)
plt.show()
```


![png](output_30_0.png)



```python
plt.figure(figsize = (6,4))
# print(df)
meanRain=tourssum['RR_sum'].mean()
# medianRain=tourssum['RR_sum'].median()
# tourssum['Rain']=(tourssum['RR_sum']>meanRain).map({True:'Wet',False:'Dry'})
fig=sns.barplot(x=tourssum.index.year,y='RR_sum',hue='Rain',dodge=False,data=tourssum)
fig.axhline(meanRain, color='purple')
plt.xticks(rotation=45)
plt.show()
```


![png](output_31_0.png)


What can we say on those graphs?  
The number of warm days is significantly smaller than the average value making it a quite cold summer while the sum of the rainfalls is lower than the average, which means that summer 2021 was drier than usual.  
Can we then conclude than summer 2021 was not that bad?   
Well, we need to further study the statistics, namely, to what extend summer 2021 was colder and drier than usual. For this purpose we start by looking at the distribution of the total number of warm days in summer and the amount of the precipitation over the last thirty years. 


```python
tourssum30=summerstat(tours,1990)
t2021warm=tourssum30.loc['2021-12-31','Warm days']
fig=tourssum30['Warm days'].hist()
fig.axvline(t2021warm, color='orange')
plt.title('Distribution of # of warm days')
plt.annotate('2021',(15.5,5.5), size=13,color='orange')
plt.plot()
```




    []




![png](output_33_1.png)



```python
t2021rain=tourssum30.loc['2021-12-31','RR_sum']
fig=tourssum30['RR_sum'].hist()
fig.axvline(t2021rain, color='orange')
plt.title('Distribution of amount of precipitation')
plt.annotate('2021',(80,5.5), size=13,color='orange')
plt.plot()
```




    []




![png](output_34_1.png)


Even if we included more years, our dataset remains limited and the histograms are not easy to interpret and strongly biased by the choice of the number of bins. We can still note that for most of the years, the summer was warmer.
However, to get more quantitative conclusions, we implement the plots of the empirical cumulative distribution functions, ECDF. 

ECDF, what is it?
It takes the measures of a dataset to calculate the probability to get a value smaller or equal to x. Basically it takes all the values of a dataset, order them and for each of them return the number of measurements that gave smaller or equal values. This is what our *ecdf* function does.
On the graphs, the 2021 summer and the corresponding percentile are highlighted with the dashed lines. Finally, to check the nature of the distribution, we also plot the ecdf of the normal theoretical function of our data in orange solid line. 



```python
def ecdf(data):
    """Compute ECDF for a one-dimensional array of measurements."""
    # Number of data points: n
    n = len(data)
    # x-data for the ECDF: x
    x = np.sort(data)
    # y-data for the ECDF: y
    y = np.arange(1, n+1) / n
    return x, y
```


```python
#Preparation of ecdf plot 
def ecdfdata(data,par,year):
    x,y=ecdf(data[par])
    #Get theoretical normal ecdf with empirical mean and std to 
    #check normality of the empirical data.
    samples=np.random.normal(np.mean(data[par]),np.std(data[par]),size=10000)
    x_theor,y_theor=ecdf(samples)
    #Create a dataframe for easy access to ecdf from the x value
    tab=pd.DataFrame(y,index=x,columns=['ECDF'])
    xyear=data.loc[year,par]
    #Extract the ecdf value of the considered year. 
    print(type(tab.loc[data.loc[year,par],'ECDF']))
    #Handles the case where multiple years has the same value. 
    if type(tab.loc[data.loc[year,par],'ECDF'])==np.float64:
        yyear=(tab.loc[data.loc[year,par],'ECDF'])
    else:
        yyear=max(tab.loc[data.loc[year,par],'ECDF'])
    return x,y,x_theor,y_theor,xyear,yyear  

```


```python
def ecdfplot(x,y,x_theor,y_theor,xyear,yyear,title):
    fig, ax = plt.subplots()
    plt.plot(x,y, marker='.', linestyle='none')
    plt.plot(x_theor,y_theor, linestyle='-')
    plt.plot(xyear,yyear, marker='+')
    x0,xm=ax.get_xlim()
    y0,ym=ax.get_ylim()
    ax.vlines(xyear, ymin=y0, ymax=yyear, linestyle="dashed")
    ax.hlines(yyear, xmin=x0, xmax=xyear, linestyle='dashed')
    ax.set_xlim(x0,xm)
    ax.set_ylim(y0,ym)
    plt.annotate("({:.0f}, {:.0f}th percentile)".format(xyear,yyear*100),
                 xy=(xyear,yyear),
                 textcoords="offset points", # how to position the text
                 xytext=(10,0), # distance from text to points (x,y)
                 ha='left') # horizontal alignment can be left, right or center)
    plt.title(title)
    plt.show()
```


```python
x,y,x_theor,y_theor,xyear,yyear=ecdfdata(tourssum30,'Warm days','2021-12-31')  
ecdfplot(x,y,x_theor,y_theor,xyear,yyear,'Warm days, 2021')

```

    <class 'pandas.core.series.Series'>



![png](output_40_1.png)


## HERE
Comments:
- 2021
- normal distribution
   


```python
x,y,x_theor,y_theor,xyear,yyear=ecdfdata(tourssum30,'RR_sum','2021-12-31')  
ecdfplot(x,y,x_theor,y_theor,xyear,yyear,'Precipitations, 2021')
```

    <class 'numpy.float64'>



![png](output_42_1.png)


Comments:
- 2021
- normal distribution

Conclusion on summer 2021 in Tours, averagely wet but much less warm days as usual.

Finally, for the sake of curiosity, let me disgress a bit on the summer rainfalls. At this point, my question is: is the amount of precipitation evenly distributed in both summer months?


```python
df=toursmonthly[toursmonthly.summer==True]
df=df.assign(M=df.index.month.map({7:'july',8:'august'}))
plt.figure(figsize = (10,6))
dpi=df.pivot(columns='M', values='RR_sum').resample('Y').sum()
dpi=dpi[['july','august']]
# print(dpi.head())
dpi.set_index(dpi.index.year).plot(kind='bar', stacked=True, width=0.8)
plt.xticks(rotation=45)
plt.show()
```


    <Figure size 720x432 with 0 Axes>



![png](output_46_1.png)


Not evenly distributed on july and august

## Correlations 

We loaded a set of weather parameters, let us see if we can notice any correlations between them.
For this purpose, we calculate the correlation matrix using the Pearson method of the daily parameters. 


```python
# sns.pairplot(tours[(tours['DATE'].dt.year>1980)], diag_kind='hist', diag_kws=dict(bins=8))

```


```python
# Generate correlation matrix
corr = tours.corr(method='pearson')

# Draw the heatmap 
sns.heatmap(corr, 
              annot = True );   # Include values within squares


```


![png](output_51_0.png)


Comments:

### "Noël au balcon, Pâques aux tisons ?"
In France, there is a phrase that reads "Noël au balcon, Pâques aux tisons". It basically means that if Christmas day is warm then Easter will be a cold day. We can use our data to check this popular saying.
The first challenge is to get the date of Easter since it is not a fixed day. Fortunately, there is a Python function in _dateutil_ that does it. 


```python
#Set the range of years to consider.
r=range(1961,2021)
#Initialize a dataframe with the years as indexes.
dfcor=pd.DataFrame(index=r)
#Fill in the dataframe with the mean temperatures of Christmas and the following Easter.
for d in r:
    dfcor.loc[d,'christmas']=tours.set_index('DATE').loc[str(d)+'-12-25','TG']
    dfcor.loc[d,'easter']=tours.set_index('DATE').loc[str(easter(d)),'TG']
print(dfcor.head())
```

          christmas  easter
    1961       -1.8    10.8
    1962       -8.0    12.4
    1963       -0.7     8.4
    1964        0.9     4.8
    1965        7.8     9.4


To check the potential correlation between Christmas and Easter mean temperatures, we make a scatterplot and calculate the correlation coefficient with Pearson method.


```python
sns.scatterplot(x='christmas', y='easter', data=dfcor)
print(dfcor.corr(method='pearson'))
```

               christmas    easter
    christmas   1.000000 -0.074079
    easter     -0.074079  1.000000



![png](output_56_1.png)


The plot and the coefficient do not indicate any correlation. However, we can investigate in greater detail. The saying states that warm Christmas leads to cold Easter, which not necessarily implies a correlation of the temperatures. Let us isolate warm Christmas year and compare the Easter temperature with other years. 


```python
dfcor['warmXmas']= dfcor['christmas']>7
# print(dfcor.head())
```


```python
sns.scatterplot(x='christmas', y='easter', data=dfcor, hue='warmXmas')
```




    <AxesSubplot:xlabel='christmas', ylabel='easter'>




![png](output_59_1.png)



```python
dfcor.groupby('warmXmas')['easter'].mean()
```




    warmXmas
    False    9.780556
    True     9.504167
    Name: easter, dtype: float64



The difference of the mean Easter temperature is very small and we cannot qualify it as significant. To go further, we perform a bootstrap analysis. EXPLANATION


```python
def permutation_sample(data1, data2):
    """Generate a permutation sample from two data sets."""
    # Concatenate the data sets: data
    data =np.concatenate((data1, data2))
    # Permute the concatenated array: permuted_data
    permuted_data = np.random.permutation(data)
    # Split the permuted array into two: perm_sample_1, perm_sample_2
    perm_sample_1 = permuted_data[:len(data1)]
    perm_sample_2 = permuted_data[len(data1):]
    return perm_sample_1, perm_sample_2
def draw_perm_reps(data_1, data_2, func, size=1):
    """Generate multiple permutation replicates."""
    # Initialize array of replicates: perm_replicates
    perm_replicates = np.empty(size)
    for i in range(size):
        # Generate permutation sample
        perm_sample_1, perm_sample_2 = permutation_sample(data_1,data_2)
        # Compute the test statistic
        perm_replicates[i] = func(perm_sample_1,perm_sample_2)
    return perm_replicates
def diff_of_means(data_1, data_2):
    """Difference in means of two arrays."""
    # The difference of means of data_1, data_2: diff
    diff = np.mean(data_1)-np.mean(data_2)
    return diff
# Compute difference of mean impact force from experiment: 
empirical_diff_means = diff_of_means(dfcor[dfcor['warmXmas']==True]['easter'],dfcor[dfcor['warmXmas']==False]['easter'])
# Draw 10,000 permutation replicates: perm_replicates
perm_replicates = draw_perm_reps(dfcor[dfcor['warmXmas']==True]['easter'],dfcor[dfcor['warmXmas']==False]['easter'], diff_of_means, size=10000)
# Compute p-value: p
p = np.sum(perm_replicates >= empirical_diff_means) / len(perm_replicates)
# Print the result
print('p-value =', p)
```

    p-value = 0.6193


The difference of the respective mean of the easter average temperature of the two categories, cold christmas and warm christmas is not significative. We therefore cannot validate the popular saying.

## Machine Learning

Can we use monthly summary statistics to determine the season the month belongs to?

### Classification

Step 1: map season to month.


```python
toursmth=monthstat(tours,1980)
#We won't need summer.
toursmth.drop(['summer','SS_sum'],1,inplace=True)
toursmth.dropna(inplace=True)
toursmth['season']=(toursmth.index.month%12+3)//3
seasons = {1: 'Winter',2: 'Spring',3: 'Summer',4: 'Autumn'}
toursmth['season'] = toursmth['season'].map(seasons)
toursmth['season']=toursmth['season'].astype('category')
#Reset index
toursmth.reset_index(inplace=True)
display(toursmth.head())
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>DATE</th>
      <th>TX_mean</th>
      <th>Warm days</th>
      <th>TN_mean</th>
      <th>TG_mean</th>
      <th>RR_sum</th>
      <th>HU_mean</th>
      <th>QQ_sum</th>
      <th>PP_mean</th>
      <th>season</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1980-01-31</td>
      <td>4.696774</td>
      <td>0.0</td>
      <td>0.096774</td>
      <td>2.396774</td>
      <td>60.3</td>
      <td>82.000000</td>
      <td>128.3</td>
      <td>1015.922581</td>
      <td>Winter</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1980-02-29</td>
      <td>10.279310</td>
      <td>0.0</td>
      <td>4.224138</td>
      <td>7.255172</td>
      <td>67.1</td>
      <td>88.344828</td>
      <td>183.0</td>
      <td>1019.879310</td>
      <td>Winter</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1980-03-31</td>
      <td>9.987097</td>
      <td>0.0</td>
      <td>3.561290</td>
      <td>6.774194</td>
      <td>122.2</td>
      <td>84.096774</td>
      <td>272.5</td>
      <td>1010.680645</td>
      <td>Spring</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1980-04-30</td>
      <td>14.190000</td>
      <td>0.0</td>
      <td>4.870000</td>
      <td>9.523333</td>
      <td>12.1</td>
      <td>70.533333</td>
      <td>518.8</td>
      <td>1019.723333</td>
      <td>Spring</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1980-05-31</td>
      <td>17.796774</td>
      <td>1.0</td>
      <td>7.300000</td>
      <td>12.545161</td>
      <td>92.0</td>
      <td>71.354839</td>
      <td>680.8</td>
      <td>1012.567742</td>
      <td>Spring</td>
    </tr>
  </tbody>
</table>
</div>



```python
sns.pairplot(toursmth, hue='season')
```




    <seaborn.axisgrid.PairGrid at 0x7f9d92256630>




![png](output_67_1.png)


Comment pairplot.

Prepare ML.



```python
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.model_selection import train_test_split
# Create feature and target arrays
X = toursmth.drop(['season','DATE'],axis=1)
y = toursmth['season']
# yd= pd.get_dummies(y)
# Split into training and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state=42, stratify=y)
```

Wise choice of the number of neighbours. 


```python
# Setup arrays to store train and test accuracies
neighbors = np.arange(1, 15)
train_accuracy = np.empty(len(neighbors))
test_accuracy = np.empty(len(neighbors))
# Loop over different values of k
for i, k in enumerate(neighbors):
    # Setup a k-NN Classifier with k neighbors: knn
    knn = KNeighborsClassifier(k)
    # Fit the classifier to the training data
    knn.fit(X_train, y_train)    
    #Compute accuracy on the training set
    train_accuracy[i] = knn.score(X_train, y_train)
    #Compute accuracy on the testing set
    test_accuracy[i] = knn.score(X_test, y_test)
# Generate plot
plt.title('k-NN: Varying Number of Neighbors')
plt.plot(neighbors, test_accuracy, label = 'Testing Accuracy')
plt.plot(neighbors, train_accuracy, label = 'Training Accuracy')
plt.legend()
plt.xlabel('Number of Neighbors')
plt.ylabel('Accuracy')
plt.show()
```


![png](output_72_0.png)


Best choice of the number of neighbors: 5.


```python
# Create a k-NN classifier with 5 neighbors: knn
knn5 = KNeighborsClassifier(5)
# Fit the classifier to the training data
knn5.fit(X_train, y_train)
# Print the accuracy
print(knn5.score(X_test, y_test))
# Create a k-NN classifier with 7 neighbors: knn
knn7 = KNeighborsClassifier(7)
# Fit the classifier to the training data
knn7.fit(X_train, y_train)
# Print the accuracy
print(knn7.score(X_test, y_test))
```

    0.7373737373737373
    0.7272727272727273


Educated choice of the number of neighbors, however, there is still a bias due to the train/test splitting. 


```python
# Import the necessary modules
from sklearn.model_selection import cross_val_score
# Compute 5-fold cross-validation scores: cv_scores
cv_scores = cross_val_score(knn5, X, y, cv=10)
# Print the 5-fold cross-validation scores
print(cv_scores)
print("Average 10-Fold CV Score with 5 neighbors: {}".format(np.mean(cv_scores)))
```

    [0.78846154 0.80769231 0.63461538 0.75       0.72916667 0.77083333
     0.79166667 0.77083333 0.8125     0.64583333]
    Average 10-Fold CV Score with 5 neighbors: 0.7501602564102564


Possible improvement with scaling? we considered many features whose variance can be different from one to another, so rescaling the values of the features before fitting a model might improve our accuracy. 


```python
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
# Setup the pipeline steps: steps
steps = [('scaler', StandardScaler()),
        ('knn', KNeighborsClassifier())]
# Create the pipeline: pipeline
pipeline = Pipeline(steps)
pipeline.fit(X_train, y_train)
# Print the accuracy
print(pipeline.score(X_test, y_test))
cv_scores = cross_val_score(pipeline, X, y, cv=10)
# Print the 5-fold cross-validation scores
print(cv_scores)
print("Average 10-Fold CV Score with 5 neighbors: {}".format(np.mean(cv_scores)))
y_pred=pipeline.predict(X_test)
```

    0.7777777777777778
    [0.80769231 0.80769231 0.78846154 0.89583333 0.79166667 0.91666667
     0.91666667 0.85416667 0.85416667 0.70833333]
    Average 10-Fold CV Score with 5 neighbors: 0.8341346153846153


Improved accuracy.


```python
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
# Instantiate LogisticRegression
logreg = LogisticRegression(
    solver='liblinear',
    random_state=42
)

# Train the model
logreg.fit(X_train, y_train)

print(logreg.score(X_test, y_test))
cv_scores = cross_val_score(logreg, X, y, cv=10)
# Print the 5-fold cross-validation scores
print(cv_scores)
print("Average 10-Fold CV Score with 5 neighbors: {}".format(np.mean(cv_scores)))
# # AUC score for tpot model
# logreg_auc_score = roc_auc_score(y_test, logreg.predict_proba(X_test)[:, 1])
# print(f'\nAUC score: {logreg_auc_score:.4f}')
```

    0.8383838383838383
    [0.86538462 0.84615385 0.84615385 0.91666667 0.8125     0.91666667
     0.83333333 0.9375     0.875      0.75      ]
    Average 10-Fold CV Score with 5 neighbors: 0.8599358974358975


Identify the wrongly assigned months? 


```python
dfcheck=toursmth.loc[y_test.index,['DATE','season']]
dfcheck['seasonpred']=y_pred
dfcheck['Fail']=np.where(dfcheck["season"] != dfcheck["seasonpred"], True, False)
display(dfcheck.head(3))
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>DATE</th>
      <th>season</th>
      <th>seasonpred</th>
      <th>Fail</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>96</th>
      <td>1988-01-31</td>
      <td>Winter</td>
      <td>Winter</td>
      <td>False</td>
    </tr>
    <tr>
      <th>356</th>
      <td>2009-09-30</td>
      <td>Autumn</td>
      <td>Autumn</td>
      <td>False</td>
    </tr>
    <tr>
      <th>409</th>
      <td>2014-02-28</td>
      <td>Winter</td>
      <td>Spring</td>
      <td>True</td>
    </tr>
  </tbody>
</table>
</div>



```python
dfcheck['month']=dfcheck.DATE.dt.month
dfmthcheck=dfcheck.groupby('month')['Fail'].count()
dfmthcheck.plot(kind='bar')
```




    <AxesSubplot:xlabel='month'>




![png](output_83_1.png)


Not enough data to draw conclusion on the efficiency of KNN with respect to the month. 

### K-means clustering.


```python
# Import KMeans
from sklearn.cluster import KMeans
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
ks = range(1, 6)
inertias = []
for k in ks:
    # Create a KMeans instance with k clusters: model
    model=KMeans(n_clusters=k)
    # Fit model to samples
    model.fit(X)
    # Append the inertia to the list of inertias
    inertias.append(model.inertia_)
plt.plot(ks, inertias)    
```




    [<matplotlib.lines.Line2D at 0x7f9d67ac5588>]




![png](output_86_1.png)



```python
# Create a KMeans model with 4 clusters:
model = KMeans(n_clusters=4)
# Use fit_predict to fit model and obtain cluster labels: 
labels = model.fit_predict(X)
# Create a DataFrame with labels and season as columns: 
dfml = pd.DataFrame({'labels': labels, 'season': y})
# Create crosstab: 
ctml = pd.crosstab(dfml['labels'], dfml['season'])
print(ctml)
```

    season  Autumn  Spring  Summer  Winter
    labels                                
    0           53      74       6       2
    1            1       2       3       0
    2           67       2       0     120
    3            2      45     114       1



```python
# Create scaler: scaler
scaler = StandardScaler()
# Create KMeans instance: kmeans
kmeans = KMeans(n_clusters=4)
# Create pipeline: pipeline
pipeline = make_pipeline(scaler, kmeans)
labels = pipeline.fit_predict(X)
# Create a DataFrame with labels and season as columns: 
dfpl = pd.DataFrame({'labels': labels, 'season': y})
# Create crosstab: 
ctpl = pd.crosstab(dfpl['labels'], dfpl['season'])
print(ctpl)
```

    season  Autumn  Spring  Summer  Winter
    labels                                
    0           24      20       0      86
    1           47      69      26       1
    2           36      28       0      36
    3           16       6      97       0



```python
dfml2 =  pd.DataFrame({'labels': labels, 'month': toursmth.DATE.dt.month})
ctml2 = pd.crosstab(dfml2['labels'], dfml2['month'])
print(ctml2)
```

    month   1   2   3   4   5   6   7   8   9   10  11  12
    labels                                                
    0       29  31  20   0   0   0   0   0   0   0  24  26
    1        0   1   8  30  31  17   3   6  23  24   0   0
    2       12   9  13  10   5   0   0   0   2  17  17  15
    3        0   0   0   1   5  24  38  35  16   0   0   0

